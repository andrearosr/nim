var Animation = function(imageSequence, duration, x, y, width, height, canvas, loop){
    var canvasCtx = canvas.getContext('2d');
    var animationEnded = false;
    
    var state = 0;
    var stateCounter = 0;
    var transitionInterval;
    var loop = loop || false;
    
    transitionInterval = duration*60/imageSequence.length;
    
    this.transition = function(){
        stateCounter++;
        state = Math.round(stateCounter/transitionInterval);
        
        if(state === imageSequence.length){
            if(loop){
                state = 0;
                stateCounter = 0;
            } else animationEnded = true;
        }
    };
    
    this.render = function(){
        if(!animationEnded) canvasCtx.drawImage(imageSequence[state], x, y, width, height);
    };
};