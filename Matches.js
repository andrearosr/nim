var Matches = function(quantity, x, y, width, height, canvas){
	var ctx = canvas.getContext('2d');
	var positions = [];
	var lineSize = 1;
	var lineCounter = 0;
	var posX = x;
	var posY = y;
	var xOffset = 2*width;
	var yOffset = 2* height;

	var unused = quantity;
	var used = 0;

	for(var i = 0; i < quantity; i++){
		positions[i] = [];
		positions[i][0] = posX + (xOffset*lineCounter++);
		positions[i][1] = posY;
		if(lineCounter === lineSize){
			lineCounter = 0;
			lineSize++;
			(lineSize%2 === 0) ? posX -= xOffset/2 :  posX = x - Math.floor(lineSize/2)*xOffset;
			posY += yOffset;
		}
	}

	this.use = function(usedQuantity){
		if(unused > 0 && unused >= usedQuantity){
			var usedPositions = [];
			for(var i = unused - 1; (unused - usedQuantity) < i + 1; i--){
				usedPositions.push(positions[i]);
			}

			unused -= usedQuantity;
			used += usedQuantity;

			return usedPositions;
		}
	};

	this.getUsed = function(){
		return used;
	}

	var matchSprite = new Image();
	matchSprite.src = 'Sprites/match.png';
	var usedMatchSprite = new Image();
	usedMatchSprite.src = 'Sprites/match2.png';

	this.render = function(){
		for(var i = 0; i < quantity; i++){
			if(i < unused) ctx.drawImage(matchSprite, positions[i][0], positions[i][1], width, height);
			else ctx.drawImage(usedMatchSprite, positions[i][0], positions[i][1], width, height);
		}
	};

};