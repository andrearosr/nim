var UI = function(canvas){
	var ctx = canvas.getContext('2d');
	var components = [];
	var self = this;
	var visibility = false;

	this.createComponent = function(name, size, x, y, color, highlightColor){
		components.push({
			id: name,
			callbackFn: null,
			callbackParam: null,
			width: size[0] || size,
			height: size[1] || size,
			x: x,
			y: y,
			color: color,
			altColor: highlightColor,
			text: '',
			textFont: '',
			textColor: null,
			textAltColor: null,
			textX: 0,
			textY: 0,
			collision: false, 
			visibility: true
		});
	};

	this.render = function(){
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		for(var e in components){
			ctx.fillStyle = (components[e].collision) ? components[e].altColor : components[e].color;
			ctx.fillRect(components[e].x, components[e].y, components[e].width, components[e].height);

			ctx.fillStyle = (components[e].collision) ? components[e].textAltColor : components[e].textColor;
        	ctx.font = components[e].textFont;
        	ctx.fillText(components[e].text, components[e].textX, components[e].textY);
		}
	};

	this.visible = function(){
		visibility = true;
		self.render();
	};

	this.invisible = function(){
		visibility = false;
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	};

	this.isColliding = function(movementEvent){
		var mouseX = movementEvent.clientX;
		var mouseY = movementEvent.clientY;

		if(visibility){
			for(var e in components){
				if(mouseY > components[e].y)
					if(mouseY < components[e].y + components[e].height)
						if(mouseX > components[e].x)
							if(mouseX < components[e].x + components[e].width){
								components[e].collision = true;
								continue;
							}

			components[e].collision = false;
			}

			self.render();
		}
	};

	this.collision = function(){
		if(visibility){
			for(var e in components){
				if(components[e].collision){
					components[e].callbackFn(components[e].callbackParam);
				}
			}
		}
	};

	this.setCallback = function(name, callbackFn, param){
		for(var e in components){
			if(components[e].id === name){
				components[e].callbackFn = callbackFn;
				components[e].callbackParam = param || null;
			}
		}
	};

	this.setText = function(name, text, font, x, y, color, highlightColor){
		for(var e in components){
			if(components[e].id === name){
				components[e].text = text;
				components[e].textFont = font;
				components[e].textX = components[e].x + x;
				components[e].textY = components[e].y + y;
				components[e].textColor = color;
				components[e].textAltColor = highlightColor;
			}
		}
	};

	this.setTextColor = function(name, color, highlightColor){
		for(var e in components){
			if(components[e].id === name){
				components[e].textColor = color;
				components[e].textAltColor = highlightColor;
			}
		}
	}

	this.components = function(){
		for(var e in components){
			console.log(components[e]);
		}
	};

	canvas.addEventListener('mousemove', self.isColliding, event);
	canvas.addEventListener('mousedown', self.collision);
};